require 'green_shoes'

#  def Billboard.ask_admin_password
    Shoes.app width: 280, height: 180, title: " " do
    background black
      stack do
        stack height: 40 do
          #spacer
        end
        flow do
          @edit1 = edit_line width: 6, margin_left: 136
        end
        stack height: 30 do
          #spacer
        end
        @statusflow = flow do
          @status = para "denied", stroke: red, align: 'center'
        end
      end
      keypress do |k|
        if k == "\n"
          if @edit1.text == "toney"
            @statusflow.clear{@status = para "proceed", stroke: limegreen, align: 'center' }
            #also fire off window and change adminstatus to admin or set it to 1 and not 0
          else
            @edit1.text = ""
            #later on find a way for the denied para to hide for a second and reappear
          end
        end
      end
    end

