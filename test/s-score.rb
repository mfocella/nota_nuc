

ssfile = ARGV.first

def print_scores(file)
  names = []
  items = [] 
  input_file = file 
  IO.readlines("#{file}").each do |line|
    entries = line.split(';')
    names = entries[0]
    items = entries[1]
    names_array = []
    items_array = []
    names_array = names.split(',')
    items_array = items.split(',')
     @all_scores = []
     names_array.each do |name|
       items_array.each do |item|
        lettersinitem = item.scan(/[a-zA-Z]/).length
        @item_factors = 0
        @name_factors = 0
        @ss_multiplier = 1
        @ss_score = 0
        (1..lettersinitem).each do |n| 
          if lettersinitem % n == 0 
            @item_factors += 1
          end
        end
        (1..name.length).each do |n|
	  if name.length % n == 0
	    @name_factors += 1
	  end
	end
        if @item_factors == @name_factors
	  @ss_multiplier = 1.5
	end
	if lettersinitem.even? == true
          vowels = []
	  vowels = name.scan(/[aeiou]/)
	  numovowels = vowels.length
	  ss = numovowels * 1.5 * @ss_multiplier
	  finalss = '%.2f' % ss
	  num = finalss.to_f
	  if num > @ss_score
	    @ss_score = num
            @all_scores << @ss_score
	  end
	else
          vowels = []
	  vowels = name.scan(/[aeiou]/)
	  numovowels = vowels.length
	  consinants = name.length - numovowels
	  finalss = consinants * @ss_multiplier
	  if finalss > @ss_score
	    @ss_score = finalss
	    @all_scores << @ss_score
	  end
	end
      @total_ss_score = 0
      if @total_ss_score < @ss_score
	@total_ss_score = @ss_score
      end
    end
  end
  largest_score = @all_scores.sort.last
  float_largest = '%.2f' % largest_score
  puts "#{float_largest}"
end
end
print_scores(ssfile)
