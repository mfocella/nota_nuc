#Quarrier is a module which accesses the Penrhyn database using activerecord calls, updates and additions

=begin
Activerecord
The MIT License (MIT)
Copyright (c) <year> <copyright holders>
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
=end

lib_path = File.expand_path("../../lib", __FILE__)
$:.unshift lib_path unless $:.include? lib_path

module Quarrier
require 'active_record'
require 'Billboard_module.rb'
include Billboard
require 'logger'



ActiveRecord::Base.establish_connection({
adapter: 'sqlite3',
database: File.expand_path("../../db/Penrhyn.db", __FILE__),
#user: 'randomstring',    '../db/Penrhyn.db',   (this will be added later if database gets encrypted)
#password: 'randomtext', (this will be used later if database gets encrypted)
host: 'localhost'
})

 
class Slate < ActiveRecord::Base
  validates :slatename, length: { in: 2..18, message: "The length of the slate name must be between 1 and 18" }
  validates :slatename, uniqueness: { case_sensitive: true, message: "The slate name must be unique (this check is case sensitive)" }
  validates :slatename, :connectionstatus, :commandonly, :viewing, presence: true
  validates :maintext, length: { maximum: 200000, message: "The length of the slates text must not exceed 200000 characters"}
  validates :connectionstatus, :commandonly, :viewing, length: { in: 0..1, message: "The connectionstatus must be either 0 or 1" }


  def self.error_handler_slatename(record)
      if record.invalid?
        logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
        record.errors.each do |attr, msg|
          if "#{attr}" == "slatename"
            Billboard.alert_slatename_error
            logger.info "#{msg}"
          end
        end
        logger.close
        record.errors.clear
      end
  end

end




class GeneralSetting < ActiveRecord::Base
  validates :settings, :updatetime, :lockslatecheck, :networkcheck, presence: true
  validates :locktime, length: { in: 1..2, message: "The locktime length must be between 1 and 99" }
  validates :locktime, numericality: { only_integer: true }
  validates :updatetime, length: { in: 1..2, message: "The updatetime length must be between 1 and 99" }
  validates :updatetime, numericality: { only_integer: true }
  validates :lockslatecheck, :networkcheck, length: { in: 0..1, message: "The lockslatecheck length must be either 0 or 1" }
  validates :adminpassword, length: { in: 14..50, message: "The admin password must have between 14 and 50 characters" }

  def self.error_handler_lock(record)
    if record.invalid?
      logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
      record.errors.each do |attr, msg|
        if "#{attr}" == "locktime"
          Billboard.alert_locktime_error
          logger.info "#{msg}"
        end
      end
      logger.close
      record.errors.clear
    end
  end

  def self.error_handler_utime(record)
    if record.invalid?
      logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
      record.errors.each do |attr, msg|
        if "#{attr}" == "updatetime"
          Billboard.alert_updatetime_error
          logger.info "#{msg}"
        end
      end
      logger.close
      record.errors.clear
    end
  end

  def self.error_handler_password(record)
    if record.invalid?
      logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
      record.errors.each do |attr, msg|
        if "#{attr}" == "adminpassword"
          Billboard.alert_password_error
          logger.info "#{msg}"
        end
      end
      logger.close
      record.errors.clear
    end
  end
end


class Host < ActiveRecord::Base
  validates :hostname, length: { in: 2..40, message: "The name of a host must be between 2 and 50 characters in length" }
  validates :hostname, presence: true
  validates :hostname, uniqueness: { case_sensitive: true, message: "The hostname must be unique (this check is case sensitive)"}

#the below error_handler method needs to be fixed
  def error_handler(record)
    if record.errors.invalid?
      if record.errors.added? :hostname do
        Billboard.alert_hostname_error
        logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
        logger.info "#{record.errors.messages}"
        logger.close
      end
    end
    record.errors.clear
    end
  end
end



class SlateHost < ActiveRecord::Base
  validates :hostid, :slateid, presence: true
  #add here to check that both :slateid and :hostid exist in their respective databases
end

class Status < ActiveRecord::Base
  validates :state, :isadmin, :newtext, :doppelganger, :giftoffers, :cmdlineresponse, presence: true
end


  def nextslate 
    sv = Slate.find_by(viewing: 1)
    svn = sv.slatename
    slates = []
    slates = list_slates
    index = slates.find_index("#{svn}")
    nextindex = index + 1
    nextslate = slates[nextindex]
    if slates.last == "#{svn}"
      return slates[0]
    else
      return nextslate
    end
  end  


  def previousslate
    sv = Slate.find_by(viewing: 1)
    svn = sv.slatename
    slates = []
    slates = list_slates
    index = slates.find_index("#{svn}")
    previousindex = index - 1
    previousslate = slates[previousindex]
    firstslate = slates[0]
    if firstslate == "#{svn}"
      return slates[-1]
    else
      return previousslate
    end
  end

  def viewing?(slate)
    argument = /^(\w+)\s*$/.match("#{slate}").captures
    arg = argument.join("")
    s = Slate.find_by(viewing: 1)
    n = s.slatename
    if "#{n}" == "#{arg}"
      return 1
    end
  end

  def set_viewing(slate)
    argument = /^(\w+)\s*$/.match("#{slate}").captures
    arg = argument.join("")
    s = Slate.find_by(viewing: 1)
    s.viewing = 0
    n = Slate.find_by(slatename: "#{arg}")
    n.viewing = 1
    s.save
    n.save
  end

#I don't think I need the below method anymore
  def set_viewing_byid(id)
    s = Slate.find_by(viewing: 1)
    s.viewing = 0
    n = Slate.find_by(slateid: "#{id}")
    n.viewing = 1
    s.save
    n.save
  end

  def set_maintext_viewing
    s = Slate.find_by(viewing: 1)
    t = s.maintext
    return t
  end

  def is_admin?
    s = Status.find_by(state: 0)
    a = s.isadmin
    return a
  end

  def set_admin(status)
    s = Status.find_by(state: 0)
    s.isadmin = "#{status}"
    s.save
  end

  def update_text(text)
    s = Slate.find_by(viewing: 1)
    s.maintext = "#{text}"
    s.save
  end

  #is below used anywhere in my program because set_maintext_viewing does the same thing so if below isn't used, delete it
  def get_text
    s = Slate.find_by(viewing: 1)
    t = s.maintext
    return t
  end

  def switch_maintext_L
    newslate = Slate.find_by(slatename: "#{previousslate}")
    s = Slate.find_by(viewing: 1)
    s.viewing = 0
    s.save
    newslate.viewing = 1
    t = newslate.maintext
    newslate.save
    return t
  end

  def switch_maintext_R
    newslate = Slate.find_by(slatename: "#{nextslate}")
    s = Slate.find_by(viewing: 1)
    s.viewing = 0
    s.save
    newslate.viewing = 1
    t = newslate.maintext
    newslate.save
    return t
  end

  def get_slatename
    s = Slate.find_by(viewing: 1)
    n = s.slatename
    return n
  end


  def set_slatename(slate_id, newslate)
    s = Slate.find_by(slateid: "#{slate_id}")
    slate = Slate.new(slatename: "#{newslate}", viewing: "0", connectionstatus: "0", maintext: "s", commandonly: "0" )
    Slate::error_handler_slatename(slate)
    old = s.slatename
    s.slatename = "#{newslate}"
    s.save
    logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
    logger.info "The slate #{old} was renamed to #{newslate}"
    logger.close
  end

  def get_slateid(name)
    n = Slate.find_by(slatename: "#{name}")
    s = n.slateid
    return s
  end

  def new_slate(name)
    slate = Slate.new
    test = Slate.new(slatename: "#{name}", viewing: "0", connectionstatus: "0", maintext: "s", commandonly: "0" )
    Slate::error_handler_slatename(test)
    slate.slatename = "#{name}"
    slate.connectionstatus = 0
    slate.commandonly = 0
    slate.viewing = 0
    slate.maintext = "#{name}"
    if slate.invalid? 
      nil
    else
      slate.save
      logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
      logger.info "The slate #{name} was created"
      logger.close
    end
  end

  def list_slates
    slatenames = []
    all = Slate.select(:slatename)
    all.each { |r| slatenames << r.slatename }
    return slatenames
  end

  def list_hosts
    hostnames = []
    all = Host.select(:hostname)
    all.each { |h| hostnames << h.hostname }
    return hostnames
  end

  def remove_slate(name)
    s = Slate.find_by(slatename: "#{name}")
    s.destroy
    logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
    logger.info "The slate #{name} was removed"
    logger.close
  end

  def get_update_time
    s = GeneralSetting.find_by(settings: "0")
    t = s.updatetime
    return t
  end
  
  def set_update_time(seconds)
    s = GeneralSetting.find_by(settings: "0")
    s.updatetime = "#{seconds}"
    setting = GeneralSetting.new(locktime: "7", updatetime: "#{seconds}")
    GeneralSetting::error_handler_utime(setting)
    s.save
  end

  def get_lock_time
    s = GeneralSetting.find_by(settings: "0")
    t = s.locktime
    return t
  end
 
  def set_lock_time(minutes)
    s = GeneralSetting.find_by(settings: "0")
    s.locktime = "#{minutes}"
    setting = GeneralSetting.new(locktime: "#{minutes}", updatetime: "8")
    GeneralSetting::error_handler_lock(setting)
    s.save
  end
 
  def get_lock_state
    s = GeneralSetting.find_by(settings: "0")
    l = s.lockslatecheck
    return l
  end

  def set_lock_state(checked)
    s = GeneralSetting.find_by(settings: "0")
    s.lockslatecheck = "#{checked}"
    s.save
  end

  def get_admin_status
    s = GeneralSetting.find_by(settings: "0")
    a = s.adminstatus
    return a
  end

  def set_admin_status(checked)
    s = GeneralSetting.find_by(settings: "0")
    s.adminstatus = "#{checked}"
    s.save
  end

  def get_admin_password 
    s = GeneralSetting.find_by(settings: "0")
    p = s.adminpassword
    return p
  end

  def set_admin_password(password)
    s = GeneralSetting.find_by(settings: "0")
    s.adminpassword = "#{password}"
    setting = GeneralSetting.new(adminpassword: "#{password}", updatetime: "4")
    GeneralSetting::error_handler_password(setting)
    s.save
  end

  def get_connectivity
    s = GeneralSetting.find_by(settings: "0")
    c = s.networkcheck
    return c
  end

  def set_connectivity(connect)
    s = GeneralSetting.find_by(settings: "0")
    s.networkcheck = "#{connect}"
    s.save
  end

  def using_cmd?
    s = Status.find_by(state: "0")
    u = s.cmdlineresponse 
    return u
  end

  def set_cmd_view(state)
    s = Status.find_by(state: "0")
    s.cmdlineresponse = "#{state}"
    s.save
  end

end

#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014
