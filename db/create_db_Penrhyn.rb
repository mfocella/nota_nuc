
=begin
Sqlite3
Copyright (c) 2004, Jamis Buck (jamis@jamisbuck.org)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    * The names of its contributors may not be used to endorse or promote
      products derived from this software without specific prior written
      permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end



require 'sqlite3'
require 'logger'




TRAVEL = "Japan
eat ramen there
visit Tokyo
what city should I stay?


Seoul Korea
get a massage there
buy a bespoke suit there
go and visit the demilitarized zone
take a hike
eat some delicious sweets
do some shopping



()()()()()()()
Iceland
Maldives
Marrakesh Morocco
"

GIFTS = "Mom birdfeeder
Steve sweater
sister giftcard for amazon
aunt flashlight"

MUSIC = "songs to buy
new blu album
Gramatik - My People
OhHoney - Be Okay
Fitz & The Tanrums - The Walker
"

THOUGHTS = "Hints and Suggestions

Here are some suggestions for using this application

A good main slate to start out with is Schedule.  Once Schedule fills up create others and move notes into them.  \n

This program can be fully controlled from the keyboard.  It is a fun and quick way to navigate between slates.  Just remember n and p

The example slates can give you an idea of some ways to organize your information but you should create what works best for you.  

You can use this program to save snippets of code, create lists or any kind, or type out a few notes to yourself.  This is how it started for me :)

The following ideas have been given to me by others and I think they could be useful as ways to organize your information.
log snippets of code each in their own slate or create slates for each language
keep track of a running project at work or at home
type out a grocery list of food to buy

remember to get more information about the commands just type help"

IDEAS = "\nstart investing in stocks?\n
read more
practice singing
plan a vacation with friends to Hawaii?
try out cookie recipe with almonds, lemon and peanut butter
think of ways to save money  (food, heat, gas)
"

SCHEDULE = "Monday
grocery shopping
pickup mail

Tuesday
do laundry
water plants
run a mile
study programming algorithms

Wednesday
take out the garbage
clean the kitchen
buy some books
call Mom and ask her about coming to visit

July
take cat to vet
plan birthday party for mom
buy new shoes
fix the garage door

August
Take sister shopping
25th go to Dr. Katz at 9am

Fall
buy new tv for football?
start running every weekend at 5am
learn Korean
write a song for the guitar
play the guitar in a club
pickup a book about recipes
buy stock in ups?
adopt a dog?
"


begin

  db = SQLite3::Database.new "Penrhyn.db"

  db.execute "CREATE TABLE IF NOT EXISTS slates(slateid INTEGER PRIMARY KEY, slatename VARCHAR(18), maintext TEXT, connectionstatus INTEGER CHECK (connectionstatus >= 0 and connectionstatus <= 1), commandonly INTEGER CHECK (commandonly >= 0 and commandonly <= 1), viewing INTEGER CHECK (viewing >=0 and viewing <=1 ))"
  db.execute "INSERT INTO slates VALUES(0, 'Schedule', '#{SCHEDULE}', 0, 0, 0)"
  db.execute "INSERT INTO slates VALUES(1, 'Ideas', '#{IDEAS}', 0, 0, 0)"
  db.execute "INSERT INTO slates VALUES(2, 'Music', '#{MUSIC}', 0, 0, 0)"
  db.execute "INSERT INTO slates VALUES(3, 'Travel', '#{TRAVEL}', 0, 0, 0)"
  db.execute "INSERT INTO slates VALUES(4, 'Suggestions', '#{THOUGHTS}', 0, 0, 1)"
  db.execute "INSERT INTO slates VALUES(5, 'Gifts', '#{GIFTS}', 0, 0, 0)"
  db.execute "CREATE TABLE IF NOT EXISTS general_settings(settings INTEGER PRIMARY KEY CHECK (settings = 0), adminpassword VARCHAR, updatetime INTEGER, lockslatecheck INTEGER CHECK(lockslatecheck >= 0 and lockslatecheck <=1), locktime INTEGER, networkcheck INTEGER CHECK (networkcheck >= 0 and networkcheck <= 1), adminstatus INTEGER CHECK(adminstatus >= 0 and adminstatus <= 1))"
  db.execute "INSERT INTO general_settings VALUES(0, 'somekindpptdssan', 7, 0, 9, 0, 0)"
  db.execute "CREATE TABLE IF NOT EXISTS hosts(hostid INTEGER PRIMARY KEY, hostname VARCHAR(40))"
  db.execute "CREATE TABLE IF NOT EXISTS slatehosts(shid INTEGER PRIMARY KEY, hostid INTEGER, slateid INTEGER)"
  db.execute "CREATE TABLE IF NOT EXISTS statuses(state INTEGER PRIMARY KEY CHECK (state = 0), isadmin INTEGER CHECK (isadmin >= 0 and isadmin <= 1), newtext INTEGER CHECK (newtext >= 0 and newtext <= 1), doppelganger INTEGER CHECK (doppelganger >= 0 and doppelganger <= 1), giftoffers INTEGER CHECK (giftoffers >= 0 and giftoffers <= 1), cmdlineresponse INTEGER CHECK (cmdlineresponse >= 0 and cmdlineresponse <= 1))"
  db.execute "INSERT INTO statuses VALUES(0, 0, 0, 0, 0, 0)"


=begin
  select = db.prepare "SELECT * FROM Slates"
  rs = select.execute

  rs.each do |row|
    puts row.join "\s"
  end
=end

  puts "The ../db/Penrhyn.db database file was created.  To run this application, go to lib and run ruby final_eye.rb"


rescue SQLite3::Exception => e
  #log Database exception
  puts e
  logger = Logger.new(File.expand_path("../../log/slate.log", __FILE__), 2, 2000000)
  logger.error "This exception was found when database creation file was run (create_db_Penrhyn.rb):   #{e}"
  logger.close

  
ensure
#  select.close if select
  db.close if db
end


#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014
