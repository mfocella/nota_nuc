quarrier_path = File.expand_path("../../../db", __FILE__)
lib_path = File.expand_path("../../../lib", __FILE__)
$:.unshift quarrier_path unless $:.include? quarrier_path
$:.unshift lib_path unless $:.include? lib_path

require 'Quarrier.rb'
include Quarrier
require 'sqlite3'
require 'test/unit'
include Test::Unit::Assertions



db = SQLite3::Database.open "db/Penrhyn.db"



Given /^A user wants to switch to another slate$/ do
#make sure 3 or more slates exist and only one slate is viewed do this with sqlite3 commands
  sql = db.execute "SELECT COUNT(*) FROM slates"
  resultstr = sql.join("")
  resultnum = resultstr.to_i
  assert_operator( resultnum, :>=, 3, "There should be 3 or more slates but there are only #{resultstr} slates" )
  sql2 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
  num_viewed = sql2.length
  assert_equal( num_viewed, 1, "There should be only one slate with viewed as 1 but there are #{num_viewed} slates being viewed" )
end

Then /^A user clicks the (\w+) arrow$/ do |direction|
direc = direction.to_s
  if direc == "right"
    sql = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
    first_view = sql.join("")
    sql2 = db.execute "SELECT slatename FROM slates"
    sql2_array = []
    sql2.each do |name|
      nobrackets = name.to_s.delete "[]\""
      sql2_array << nobrackets
    end
    @index_of_view = sql2_array.index "#{first_view}"
    if sql2_array.last == "#{first_view}"
      nextindex = 0
    else
      nextindex = @index_of_view.to_i + 1
    end
    next_slatename = sql2_array[nextindex]
    switch_maintext_R
    sql3 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
    next_view = sql3.join("")
    assert_equal( next_view, next_slatename ) 
  elsif direc == "left"
    sql = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
    first_view = sql.join("")
    sql2 = db.execute "SELECT slatename FROM slates"
    sql2_array = []
    sql2.each do |name|
      nobrackets = name.to_s.delete "[]\""
      sql2_array << nobrackets
    end
    @index_of_view = sql2_array.index "#{first_view}"
    if sql2.first == "#{first_view}"
      previousindex = -1
    else
      previousindex = @index_of_view.to_i - 1
    end
    previous_slatename = sql2_array[previousindex]
    switch_maintext_L
    sql3 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
    previous_view = sql3.join("")
    assert_equal( previous_view, previous_slatename )
  else
    assert_equal( direction, nothing, "The direction left or right was not seen by the code" )
  end
end

And /^A user wants to switch back to the first slate$/ do
#make sure 3 or more slates exist and only one slate is viewed do this with sqlite3 commands
  sql = db.execute "SELECT COUNT(*) FROM slates"
  resultstr = sql.join("")
  resultnum = resultstr.to_i
  assert_operator( resultnum, :>=, 3, "There should be 3 or more slates but there are only #{resultstr} slates" )
  sql2 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
  num_viewed = sql2.length
  assert_equal( num_viewed, 1, "There should be only one slate with viewed as 1 but there are #{num_viewed} slates being viewed" )
end

And /^The program switches to the (\w+) slate$/ do |switch|
  pending("check on previous or next slate by viewing and get maintext and slatename")
end

When /^A user clicks the (\w+) button$/ do |button|
  pending("This test is waiting for A user gets a new_slate window test below to be created first")
end

Then /^A user gets a (\w+) window$/ do |window|
  pending("view the new slate window")
#how to check if window popped up?  do this one later might have to go into final_eye using some kind of check possibly minitest?
end

And /^A user names the slate pickles$/ do
#make sure 3 or more slates exist and only one slate is viewed do this with sqlite3 commands
  sql = db.execute "SELECT COUNT(*) FROM slates"
  resultstr = sql.join("")
  resultnum = resultstr.to_i
  assert_operator( resultnum, :>=, 3, "There should be 3 or more slates but there are only #{resultstr} slates" )
  sql2 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
  num_viewed = sql2.length
  assert_equal( num_viewed, 1, "There should be only one slate with viewed as 1 but there are #{num_viewed} slates being viewed" )
end

Then /^A user hits the ok button$/ do
  #pending("run methods that are run when new slate window ok button clicked")
  #new_slate("pickles")
end

Then /^A slate is created with the name pickles$/ do
  pending("check new slate created with same name and settings")
  sql = db.execute "SELECT slatename FROM slates"
  slate = sql.include? 'pickles'
  assert slate, "The Penrhyn database was expected to have the slatename pickles in it"
end

Given /^A user inputs (\w+) into schedule (\w+)$/ do |text, slate|
  pending("user inputs text")
#set_viewing("schedule")
#scheduletext = set_maintext_viewing
#updatedtext = "#{scheduletext}" + "#{text}"
#updatetext("#{updatedtext}")
#how to check if text is inputted into the slate?
#would I just do an update slate here? after getting the text from the db and then adding it and then running update text command from quarrier that final eye uses 
end

Then /A user goes back to the first slate and the text is still there$/ do |text|
  pending("check user didn't lose text when go back to first slate")
end

Then /^A user exits the application by clicking the x in the corner$/ do |exit|
  pending("exit the application")
#I will work on this one later when I know how to test it.  
end

When /^A user turns on the application the text is saved in that slate$/ do |start|
  pending("start the application")
#I will work on this later once I know how to test if stuff is saved if program is stopped
end

Given /^A user wants to go to the next slate$/ do
#make sure 3 or more slates exist and only one slate is viewed do this with sqlite3 commands
  sql = db.execute "SELECT COUNT(*) FROM slates"
  resultstr = sql.join("")
  resultnum = resultstr.to_i
  #resultstr = exe.next.join("")
  #resultnum = resultstr.to_i
  assert_operator( resultnum, :>=, 3, "There should be 3 or more slates but there are only #{resultstr} slates" )
  sql2 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
  num_viewed = sql2.length
  assert_equal( num_viewed, 1, "There should be only one slate with viewed as 1 but there are #{num_viewed} slates being viewed" )
end

Then /^A user clicks the Ctrl-Right button$/ do
  pending("run the method which runs when you click Ctrl-Right")
  sql2 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
  @first_view = sql2.join("")
  #switch_maintext_R
end

And /^The program switches to the next slate$/ do |slate|
  pending("check that the slate is the one that is next")
      sql = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
    second_view = sql.join("")
    sql2 = db.execute "SELECT slatename FROM slates"
    index_of_view = sql2.index"#{@first_view}"
      if sql2.last == "#{@first_view}"
        nextindex = 0
      else
        nextindex = index_of_view + 1
      end
    next_slatename = sql2[nextindex]
    sql3 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
    next_view = sql3.join("")
    assert_equal( next_view, next_slatename, "The next viewed slatename should be #{next_slatename} but was found to be #{next_view}" )
end

Given /^A user wants to access the (\w+) from the (\w+)$/ do |first, second|
  pending("tests will be created in final_eye.rb for this")
end

Then /^A user clicks the tab button$/ do |keypress|
  pending("tests will be created in final_eye.rb for this")
end

And /^The user can type in the slatetext$/ do |keypress|
  pending("tests will be created in final_eye.rb for this")
end

And /^The commandline is highlighted$/ do |keypress|
  pending("tests will be created in final_eye.rb for this")
end

Given /^A user has some slates and wants to switch to another slate$/ do |number|
pending("make sure the user has 3 slates")
  sql = db.execute "SELECT COUNT(*) FROM slates"
  resultstr = sql.join("")
  resultnum = resultstr.to_i
  #resultstr = exe.next.join("")
  #resultnum = resultstr.to_i
  assert_operator( resultnum, :>=, 3, "There should be 3 or more slates but there are only #{resultstr} slates" )
  sql2 = db.execute "SELECT slatename FROM slates WHERE viewing='1'"
  num_viewed = sql2.length
  assert_equal( num_viewed, 1, "There should be only one slate with viewed as 1 but there are #{num_viewed} slates being viewed" )
end






###   cleanup
# remove_slate("pickles")  !!!!remove pickles slate entry here using mysql code!!!! 
#  !!!!remove family-night from schdeule !!!!
#  !!!!remove bigL from Music !!!!


#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

