
Given /^my password is (\w+)$/ do |password|
  pending("I need to design the password functionality")
end

And /^I enter (\w+) at login$/ do |password|
  pending("I need to test if password works")
end

Given /^administrative password is set as Tau5slmper123ASD8809$/ do |pass|
  pending("something")
end

And /^I enter super in the command line$/ do |cmd|
  pending("I need to run the super command by inputting super into the command parsing method")
end

And /^I enter Tau5slmper123ASD8809 in the password window$/ do |password|
  pending("I need to pass above command into method called by editline in password window")
end

Then /^I should be able to open general settings$/ do |click|
  pending("check general settings pops up")
end

And /^I click general setings$/ do |click|
  pending("open up gen settings window")
end

And /^I enter Tau5slmper123ASD8809 at popup window$/ do |ent|
  pending("put password in check where window asks")
end

Then /^I should be able to run new -s pinky$/ do |command|
  pending("run above command and check pinky slate was created")
end

Then /^I get general settings window$/ do |hitok|
  pending("check general settings window popped up")
end


#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

