Feature: Commands

Users have quick access to functions and customization of slates and the application through the command line.  The commands allow for all the same functions that can be done through the gui and some additional ones.  There are both simple commands as well as more advanced commands available to the user.  Some of the dangerous commands are only available once a password is entered(superuser) 


#run a script that creates a set database of slates so I can check their text in here with that db???



Scenario: I want to run a command
When I run the command, I should get the following response
| command                | response         |
| new                    | The new command requires an option and an argument  |
| new -s carrot          | The slate carrot has been created.                  |
| new dsaa il            | This command allows you to create a new slate       |
| new dfs**()            | This command allows you to create a new slate       |
| new -s &(sas           | The slatename can't contain special characters      |
| new -s a               | Slatename must be more than 2 characters long       |
| new -s exit            | Your slate cannot be the name of a command          |
| new -s carrot          | The slate carrot already exists                     |
| new --slate carrot     | This command allows you to create a new slate       |
| new --slate &(sas      | The slatename can't contain special characters      |
| new --slate a          | Slatename must be more than 2 characters long       |
| new --slate exit       | Your slate cannot be the name of a command          |
| new --slate parrot     | This command allows you to create a new slate       |
| delete vacation        | This command allows you to delete a slate           |
| delete                 | was deleted.                                        |
| delete -s              | This command allows you to delete a slate           |
| delete -s dfsad        | The argument dfsad is not a slate                   |
| delete -s dsf@#@!      | This command allows you to delete a slate           |
| delete -s Music        | The slate Music wsa deleted                         |
| delete -s Music        | The argument Music is not a slate                   |
| delete fdsfds          | This command allows you to delete a slate           |
| delete fdsf**)         | This command allows you to delete a slate           |
| delete --slate Food    | This command allows you to delete a slate           |
| delete --slate         | This command allows you to delete a slate           |
| delete --slate dfsad   | This command allows you to delete a slate           |
| delete --slate dsf@#@! | This command allows you to delete a slate           |
| delete --slate Food    | This command allows you to delete a slate           |
| version                | You are running Version                             |
| version fdsa           | This command displays the version of this program   |
| version dfs**          | This command displays the version of this program   |
| lastlog                | INFO                                                |
| lastlog dfas           | This command shows you the last few                 |
| lastlog fds*()()       | This command shows you the last few                 |
| history                | lastlog                                             |
| history help           | This command gives you a listing of the last 50     |
| history h*()()2        | This command gives you a listing of the last 50     |
| list                   | Your slates include:                                |
| list help              | This command lists the slates you have              |
| ls                     | Your slates include:                                |
| ls fdsassa             | It is a shortcut for list                           |
| ls S*()                | It is a shortcut for list                           |
| next                   |!after install program know which slate will be next |
| next dfsaas            | This command brings you to the next slate in your   |
| next ds*&              | This command brings you to the next slate in your   |
| n                      |!go to next slate |
| n fdsas                | This command brings you to the next slate in your   |
| n fds**()              | This command brings you to the next slate in your   |
| previous               |!previous slate   |
| previous fdsas         | This command brings you to the previous slate       |
| previous dfs%$$        | This command brings you to the previous slate       |
| p                      |!previous slate   |
| p fdsas                | This command brings you to the previous slate       |
| p dfsa**&              | This command brings you to the previous slate       |
| viewslate              |!find the slate viewing with method and match viewslate command view the slate |
| viewslate fdsafsd      | This command removes the command output and lets    |
| viewslate ds*)&&       | This command removes the command output and lets    |
| v                      |!find the slate viewing with method and match viewslate command view the slate |
| v dfsaf                | This command removes the command output and lets    |
| v fds*&                | This command removes the command output and lets    |
| schedule               | take cat to vet                                     |
| schedule dsasa         | This is not a valid command or slate.               |
| Schedule dsf*()        | This is not a valid command or slate.               |
| help                   | The commands included with this program are:        |
| help dsas              | Type in help if you need assistance.                |
| help dsa*()()          | Type in help if you need assistance.                |
| super                  |!become supuser   |
| super dassdf           |!super help       |
| super dfs*&            |!super help       |
| exit help              | This command exits the program.                     |
| exit s*()*7            | This command exits the program.                     |
| e help                 |  This is not a valid command or slate.              |
| e s*()*7               |  This is not a valid command or slate.              |
| demote                 |!not admin anymore|
| demote dsasa           |!demote help      | 
| demote s*()            |!demote help      |
| rename                 | The rename command requires two arguments.          |
| rename cars            | The first argument cars is not the name of a slate  |
| rename dfsa***         | You do not have a slate named                       |
| rename Music schedule  | You already have a slate called schedule            |
| rename Music fds*&&    | You can only use letters, numbers and underscores   |
| rename Music ad        | The slatename must be more than 2 characters long   |
| rename Music exit      | Your slate cannot be the name of a command          |
| rename Music Songs     | The slate Music has changed its name to Songs       |
| about                  | This program is meant provide you with              |
| about fdsfsda          | This command gives you information about Slates.    |
| about fds**$           | This command gives you information about Slates.    |
| ThankYou               | Thank you matz for the lovely language that is ruby.|
| newz                   | This command allows you to create a new slate       |
| news ideas             | This command allows you to create a new slate       |
| new ideas -b ctrl+i    | This command allows you to create a new slate       |
| deleter vacation       | This command allows you to delete a slate           |
| deletealll             | This command allows you to delete a slate           |
| deletwe                | This is not a valid command or slate.               |
| listf                  | This command lists the slates you have.             |
| listz -h               | This command lists the slates you have.             |
| ideassdxdss            | This is not a valid command or slate                |
| fds*s@                 | This is not a valid command or slate                |
| helppp                 | Type in help if you need assistance.                |
| superrr                | This command asks for the administrator password    |



#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

