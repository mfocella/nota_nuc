Feature: Offline_General_Functionality

This application has some general functionality which can be used in an offline capacity.  These features include switching between slates with buttons, saving slates automatically, creating new slates, and renaming slates.

To add a new slate a user clicks the gift button when not lit.  This brings up the New Slate window.  Now a user can create a new slate and give it a name, shortcut, password and choose to share it with other accepted hosts.



Scenario: Switching between slates with right arrow button
  Given A user wants to switch to another slate
  Then A user clicks the right arrow
  And A user wants to switch back to the first slate 
  Then A user clicks the left arrow 
  And The program switches to the next slate

Scenario: Switching between slates with left arrow button
  Given A user wants to switch to another slate
  Then A user clicks the left arrow
  And A user wants to switch back to the first slate
  Then A user clicks the right arrow 
  And The program switches to the previous slate

Scenario: Switching from maintext to commandline with keyboard
  Given A user wants to access the commandline from the slatetext
  Then A user clicks the tab button
  And The commandline is highlighted
  
Scenario: Switching from commandline to maintext with keyboard
  Given A user wants to access the slatetext from the commandline
  Then A user clicks the tab button
  And The user can type in the slatetext

Scenario: Switching to the next slate with Ctrl-Right keyboard button
  Given A user wants to go to the next slate
  Then A user clicks the Ctrl-Right button
  And The program switches to the next slate

Scenario: Create a new slate with the new slate button
  When A user clicks the new_slate button
  Then A user gets a new slate window
  And A user names the slate pickles
  Then A user hits the ok button
  Then A slate is created with the name pickles

Scenario: Access the general settings window
  When A user clicks the general_settings button
  Then The user gets a general_settings window

Scenario: A users slate is saved after moving to another slate
  Given A user inputs family-night into schedule slate
  Then A user clicks the right arrow
  And A user clicks the left arrow
  Then A user goes back to the first slate and the text is still there

Scenario: A users slate is saved when the program is shutdown
  Given A user inputs bigL into Music
  Then A user exits the application by clicking the x in the corner
  When A user turns on the application the text is saved in that slate




#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

