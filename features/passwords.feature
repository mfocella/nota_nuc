Feature: passwords

Passwords are used to protect the security of the slates and settings.  We need to make sure to give users help when they enter an invalid password.  We also need to make sure the functionality is working correctly to protect the security of users data.



Scenario: Unlock general settings with super command
  Given administrative password is set as Tau5slmper123ASD8809
  And I enter super in the command line
  And I enter Tau5slmper123ASD8809 in the password window
  Then I should be able to open general settings 

Scenario: Unlock new command with super command
  Given administrative password is set as Tau5slmper123ASD8809
  And I enter super in the command line
  And I enter Tau5slmper123ASD8809 in the password window
  Then I should be able to run new -s pinky

Scenario: Enter general settings by click with correct password
  Given administrative password is set as Tau5slmper123ASD8809
  And I click general settings
  And I enter Tau5slmper123ASD8809 at popup window
  Then I get general settings window

Scenario: Block general settings with super command

Scenario: Block new command with super command

Scenario: Block general settings with click





#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

