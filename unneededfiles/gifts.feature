Feature: Gift button

A user needs to know when a host wants to share a slate.  This "Gift" offered by another host causes the gift button to glow.  When you click on the glowing gift button, you are shown a window which lists any slates that are offered to be shared.  You can then accept or reject those offered slates from that window.  
When you don't have any "Gifts" and click on the button you should get a window which lists the slates names, shortcuts and if a slate is shared.  It also asks at the bottom do you want to share this slate?


#gifts can be shared in the settings window as well




Scenario: A host wants to share a slate with another computer
  Given There are no gift offers
  And The gift button is not glowing  
  When A user has a slate called music
  And They want to share it
  Then They click the gift button
  And A list of slates appear
  When They check off the music slate and click yes to share
  And They check off the laptopone host and click send gift offer
  Then A gift offer is sent





Scenario: The gift button is glowing and the user wants to accept the slate
  Given There are gift offers
  And The gift button is glowing
  Then They click the gift button
  And The accept gift window appears
  When A gift is accepted
  Then The shared slate appears



Scenario: The gift button is glowing and the user wants to reject the slate
  Given There are gift offers
  And The gift button is glowing
  Then They click the gift button
  And The accept gift window appears
  When A gift is rejected 
  Then The shared slate does not appear






Scenario: The gift button is glowing and the user wants to accept some offered gifts
  Given There are gift offers
  And The gift button is glowing
  Then They click the gift button
  And The accept gift window appears
  When Some gifts are accepted
  Then Only the accepted slates should appear








#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

