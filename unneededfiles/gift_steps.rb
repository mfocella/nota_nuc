


Given /^There (are|are no) gift offers$/ do |offers|
pending("no gift offers pending")
end

And /^The gift button (is|is not) glowing$/ do |view|
pending("non glowing button showing")
end

When /^A user has a slate called (\w+)$/ do |slate|
pending("check a slate called ... exits")
end

And /^They want to share it$/ do |yes|
pending("user wants to share")
end

Then /^They click the gift button$/ do |click|
pending("share gift")
end

And /^A list of slates appear$/ do |view|
pending("check share slates window appears")
end

When /^They check off the (\w+) slate and click yes to share$/ do |slate|
pending("click yes to share the checked slate")
end

And /^They check off the (\w+) host and click send gift offer$/ do |host|
pending("do above")
end

Then /^A gift offer is sent$/ do |status|
pending("check offer was sent")
end

And /^The accept gift window appears$/ do |view|
pending("check make sure the gift window appears")
end

Then /^The shared slate appears$/ do |view|
pending("check the slate was added")
end

Then /^The shared slate does not appear$/ do |slates|
pending("check the offered slate does not appear")
end

When /^(A gift|Some gifts|No gifts|All gifts) are (accepted|rejected)$/ do |number, response|
pending("check that a or some or no gifts above")
end

Then /^Only the accepted slates should appear$/ do |slates|
pending("check that the slates appear")
end



#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

