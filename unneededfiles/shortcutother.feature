Feature: Shortcuts


#don't use below
Scenario: A user switches between slates with slate specific shortcuts
  Given A user has 4 slates and wants to switch to another slate
  And each slate has its own unique shortcut
  Then A user uses shortcut to switch between slates



Scenario: A user creates a new shortcut in the new slate window and tests it
  Given A user already has 2 other slates
  And A user creates a new slate with a shortcut in the new slate window
  And is viewing another slate
  Then A user uses shortcut to switch between slates


#don't use below
Scenario: A user changes a slate shortcut in the settings window and tests it
  Given A user already has 4 other slates
  And A user is in a slates settings window
  And the shortcut is changed to ctrl + z
  And is viewing another slate
  Then A user uses ctrl + z to switch between slates

#don't use below
Scenario: When creating a slate a user tries to use another slates shortcut
Given A user already has 4 other slates
  And A user creates a new slate with the right arrow shortcut in the new slate window
  Then you get an error message
  And A user creates a new slate with the left arrow shortcut in the new slate window
  Then you get an error message
  And A user creates a new slate with a used shortcut in the new slate window
  Then you get an error message

#don't use below
Scenario: A user tries to set a shortcut in slate settings that is already used

Given A user already has 2 other slates
  And A user alters a slate to use the right arrow shortcut
  Then you get an error message
  And A user alters a slate to use the left arrow shortcut
  Then you get an error message
  And A user alters a slate to use a used shortcut
  Then you get an error message


#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

