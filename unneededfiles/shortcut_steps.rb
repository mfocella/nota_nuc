Given /^A user has some slates and wants to switch to another slate$/ do |number|
pending("make sure the user has 3 slates")
#number = slatecount
# if number >= 3
  #passes
# else
  #fails
#end
#check the number of slates with a mysql lookup command
end


Then /^A user uses ctrl_right to switch to the slate on the right$/ do |right|
pending("method")
oldslatename = "#{get_slatename}"
text = "#{switch_maintext_right}"
slatename = "#{get_slatename}"
end

And /^The next slate is shown$/ do |slate|
  pending("make sure that the next slate is shown")
  slatename = "#{get_slatename}"
  slatetext = "#{set_maintext_viewing}"
end


=begin
And /^each slate has its own unique shortcut$/ do |slate|
pending("make sure each ...")
end

Then /^A user uses shortcut to switch between slates$/ do |slate|
pending("make sure can use shortcuts")
end

Given /^A user creates a new slate with a shortcut in the new slate window$/ do |new_slate_window|
pending("do above")
end


And /^A user already has 4 other slates$/ do |slatesnum|
pending("check number of slates")
end

And /^is viewing another slate$/ do |whatslateviewing|
pending("check users isn't on new slate and if is move to another one")
end

And /^A user is in a slates settings window$/ do |slate_settings|
pending("go to a slates settings window")
end

And /^the shortcut is changed to ctrl \+ z$/ do |shortcut|
pending("change the shortcut to ctrl \+ z")
end

And /^A user creates a new slate with the right arrow shortcut in the new slate window$/ do |rightsc|
pending("set shortcut to right arrow sc in new slate window and hit enter")
end

Then /^you get an error message$/ do |errorwin|
pending("check error window use shortcut pops up")
end


And /^A user creates a new slate with the left arrow shortcut in the new slate window$/ do |leftsc|
pending("same as right but with left")
end


And /^A user creates a new slate with a used shortcut in the new slate window$/ do |sc|
pending("same as right but with other used sc")
end

Then /^A user uses ctrl \+ z to switch between slates$/ do |sc|
pending("switch to slate with ctrl + z")
end

And /^A user alters a slate to use the right arrow shortcut$/ do |sc|
pending("set sc to right arrow sc")
end

And /^A user alters a slate to use the left arrow shortcut$/ do |sc|
pending("set sc to left arrow sc")
end

And /^A user alters a slate to use a used shortcut$/ do |sc|
pending("set sc to other set sc")
end
=end



#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

