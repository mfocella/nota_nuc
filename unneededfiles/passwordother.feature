Feature: passwords


Scenario: Login with too large of a password #larger than 30 char
  Given my password is Tau5slmper123ASD8809
  And I enter tssfrckiodltwfu32Ken!@klrcks23032Kl6 at login
  Then I should get an error saying password is wrong size


Scenario: Login with too large of a password #larger than 30 char
  Given my password is Tau5slmper123ASD8809
  And I enter 123 at login
  Then I should get an error saying password is wrong size


#checking password features within the settings window

Scenario: Setting slates password
  Given I check slates password
  And I click set button
  And I enter the password Tau5slmper123ASD8809
  Then I should be able to lock the slates program
  And  I should be able to unlock the slates with tableT


Scenario: Setting startup password
  Given I check slates startup password
  And I click set button
  And I enter the password Tau5slmper123ASD8809
  When I restart the application
  Then I should be given a locked screen
  And I should not be able to unlock the slates with tablet
  And I should not be able to unlock the slates with zz
  And I should not be able to unlock the slates with kdsjdkfk234jkj2j3k4j2k34jk23j4klj4234n3nrkefkl43knsdlsll3
  And  I should be able to unlock the slates with tableT



Scenario: Setting "admin" password
  Given I check settings password
  And I click set button
  And I enter the password tableT
  When I click the settings button
  Then I should not be able to unlock the settings with tablet
  And I should not be able to unlock the settings with 2d
  And I should not be able to unlock the settings with jkdfjk2kj54lk32j435kj46jjsd0df0f99f8skdkfkdfiikkkdk0s
  And  I should be able to unlock the settings with tableT




Scenario: Setting "lock after [_] minutes
  Given I check Lock after 10 minutes
  When 10 minutes has passed
  Then The slates should be locked





Scenario: Setting a password for a specific slate
#must test above and go into locked mode with the lock command

Scenario: Scenario typing wrong password for slate that is locked

Scenario: Scenario typing password 2 characters long and 30+ characters long


#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

