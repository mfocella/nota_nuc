Feature: Shortcuts

Shortcuts allow a user to switch between tabs.  A user can set a different shortcut for each slate and switch to that slate at anytime.  There are also shortcuts to travel right and left between slates.  These shortcuts are synonymous with the arrow key functionality.



#  arrow key shortcut is alt + arrow button on keyboard


Scenario: A user switches between slates with right control button
  Given A user has some slates and wants to switch to another slate
  Then A user uses ctrl_right to switch to the slate on the right
  And The next slate is shown








#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

