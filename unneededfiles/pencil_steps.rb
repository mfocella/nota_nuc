
And /^They have text$/ do |text|
pending("make sure each of the eight slates has text in it")
end

When /^The slate you are not viewing is changed on a shared host$/ do |view|
pending("check the slate you are viewing is not updated")
end

Then /^The pencil button glows$/ do |glow|
pending("check the pencil button is the glowing version")
end

And /^When the pencil button is clicked you are brought to the updated slate$/ do |click|
pending("check you are brought to updated slate when clicked")
end



When /^The slate you are viewing is changed on a shared host$/ do |update|
pending("change the slate from a shared remote host")
end



And /^The pencil button does not glow$/ do |view|
pending("check pencil button is showing the no glow version")
end





Given /^You have eight slates$/ do |eight|
pending("create eight slates")
end



When /^four of the slates you are not viewing are updated by a shared host$/ do |slates|
pending("update four slates from a remote host")
end

Then /^The pencil button glows$/ do |view|
pending("check the button showing is the glowing version")
end


And /^When you click on the button once, it brings you to the slate with the most recent update$/ do |click|
pending("do above")
end

And /^When you click on the button again, it brings you to the slate with the second newest update$/ do |click|
pending("do above")
end

And /^When you click on the button again, it brings you to the slate with the third newest update$/ do |click|
pending("do above")
end

And /^When you click on the button again, it brings you to the slate with the oldest update$/ do |click|
pending("do above")
end

Given /^There are no updates$/ do |update|
pending("no remote updates")
end

Then /^The pencil button is not glowing$/ do |view|
pending("make sure non glowing button version is showing")
end

When /^You click it$/ do |click|
pending("click pencil button")
end

Then /^You get a window saying there are no updates$/ do |view|
pending("make sure no update window pops up")
end

Then /^The screen is blocked and you see typing over the text area$/ do |view|
pending("make sure typing is on screen and you can't type into your slate")
end


#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

