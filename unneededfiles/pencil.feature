Feature: Pencil

This feature allows the user to know when changes have been made to a shared slate from a remote computer.  It acts as a quick way to know when things are different.  The button glows when a change has been made on another computer for one of the shared tabs.  The button does not glow when the updated slate is the one you are currently viewing.  You can click the button and be brought to that slate.  If there are multiple tabs with changes, then you will be brought to each slate in order of most recently changed until you have seen all the updated slates.  Once you have viewed each one of the updated slates the button stops glowing and becomes useless.  When you click on the button when it is not glowing you will get an annoying window telling you there were no updated slates.  




Scenario: The button glows when you have an update and you click it to see the updated slate
  Given You have eight slates
  And They have text
  When The slate you are not viewing is changed on a shared host
  Then The pencil button glows 
  And When the pencil button is clicked you are brought to the updated slate



Scenario: There was an update for the slate you are viewing and the button doesn't glow
  Given You have eight slates
  And They have text
  When The slate you are viewing is changed on a shared host
  Then The screen is blocked and you see typing over the text area
  And The pencil button does not glow


Scenario: The button glows and you click it to see multiple updates on multiple slates
  Given You have eight slates
  And They have text
  When four of the slates you are not viewing are updated by a shared host
  Then The pencil button glows
  And When you click on the button once, it brings you to the slate with the most recent update
  And When you click on the button again, it brings you to the slate with the second newest update
  And When you click on the button again, it brings you to the slate with the third newest update
  And When you click on the button again, it brings you to the slate with the oldest update


Scenario: The button is not glowing and you click it to see what it does
  Given There are no updates  
  Then The pencil button is not glowing
  When You click it
  Then You get a window saying there are no updates
  



#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

