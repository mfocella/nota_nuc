class Gift_Sent < Billboard

require 'green_shoes'

  Shoes.app height: 150, width: 280, title: " " do
    background rosybrown
#    background "#BB27FF"
    stack height: 17 do
      #spacer
    end
    tagline "One or more gifts have been sent!", align: "center", stroke: "#0E0E0E"
    stack height: 11 do
      #spacer
    end
    @b1 = button "ok", margin_left: 126
    @b1.click{exit}
  end
end


#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

