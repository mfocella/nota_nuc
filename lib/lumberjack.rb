class Lumberjack

#This class is unused at the moment



@logger = Logger.new

  def with_logging(description)
    begin
      @logger.debug( "#{description} has started")
      yield
      @logger.debug( "#{description} has completed")
    rescue
      @logger.error( "#{description} has failed")
      raise
    end
  end

  def log_before(description)
    @logger.debug( "#{description} has started")
    yield
  end

  def log_after(description)
    yield
    @logger.debug( "#{description} has completed")
  end



  def show_exception(exception, type)
    @logger.progname = "#{type}"
    @logger.error( "#{exception}")
    @logger.progname = ""
  end


end

#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

