=begin
green shoes
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
=end


quarrier_path = File.expand_path("../../db", __FILE__)
lib_path = File.expand_path("../../lib", __FILE__)
log_path = File.expand_path("../../log", __FILE__)
history_file = File.expand_path("../../log/history.log", __FILE__)
puts "#{history_file}"
$:.unshift quarrier_path unless $:.include? quarrier_path
$:.unshift lib_path unless $:.include? lib_path
$:.unshift log_path unless $:.include? log_path

require 'green_shoes'
require 'Quarrier.rb'
include Quarrier
require 'Exercitus_romanorum.rb'
include Billboard
require 'Billboard_module.rb'
include Exercitus_romanorum



#this is to keep the size of the history log file down
  #logger = Logger.new("../log/history.log", 1, 500000)
  logger = Logger.new(File.expand_path("../../log/history.log", __FILE__), 1, 500000)
  logger.close

####


  def update_history(command)
    historyfile = File.open(File.expand_path("../../log/history.log", __FILE__), "a")
    historyfile.puts "#{command}"
    historyfile.close  
  end

  def enforce_barrier
    if get_admin_status == 1
       if is_admin? == 1
         return false
       else
         return true
       end
    else
      return false
    end
  end

  def unlock_admin
  Shoes.app width: 280, height: 180, title: " " do
    background black
      stack do
        stack height: 40 do
          #spacer
        end
        flow do
          @edit1 = edit_line width: 6, margin_left: 136
        end
        stack height: 30 do
          #spacer
        end
        @statusflow = flow do
          @status = para "denied", stroke: red, align: 'center'
        end
      end
      keypress do |k|
        if k == "\n"
          if @edit1.text == "#{get_admin_password}"
            @statusflow.clear{@status = para "You now have Admin Abilities!", stroke: limegreen, align: 'center' }
            set_admin("1")
          else
            @edit1.text = ""
          end
        end
      end
    end
  end  




  def admin_barrier(win)
    if enforce_barrier
    Shoes.app width: 280, height: 180, title: " " do
    background black
      stack do
        stack height: 40 do
          #spacer
        end
        flow do
          @edit1 = edit_line width: 6, margin_left: 136
        end
        stack height: 30 do
          #spacer
        end
        @statusflow = flow do
          @status = para "denied", stroke: red, align: 'center'
        end
      end

      keypress do |k|
        if k == "\n"
          if @edit1.text == "#{get_admin_password}"
            @statusflow.clear{@status = para "proceed", stroke: limegreen, align: 'center' }
            if win == 'generalsettings_win'
              generalsettings
            elsif win == 'newslate_win'
              newslate
            elsif win == 'acceptgifts_win'
              acceptgifts
            elsif win == 'sendgifts_win'
              sendgifts
            end
            @edit1.text = ""
          else
            @edit1.text = ""
            #later on find a way for the denied para to hide for a second and reappear
          end
        end
      end
    end
  else
    if win == 'generalsettings_win'
      generalsettings
    elsif win == 'newslate_win'
      newslate
    elsif win == 'acceptgifts_win'
      acceptgifts
    elsif win == 'sendgifts_win'
      sendgifts
    end
  end
end








def generalsettings

window title: "General Settings", height: 500, width: 700 do

background "#0E0E0E"

  stack do
    flow do
      stack height: 250, width: 350 do #slates area with settings buttons 
        stack height: 40 do
          #spacer
        end
=begin
          flow do
            stack width: 200 do
              para "Slates", margin_left: 98, stroke: "#BB27FF"
              namesofslates = []
              namesofslates = list_slates 
              @slatelist_box = list_box items: namesofslates, width: 150, margin_left: 50
            end
            stack width: 150 do 
              @settingsb1 = button "settings", width: 100, height: 40, margin_left: 22, margin_top: 24
              @settingsb1.click{slatesettings("#{@slatelist_box.text}")} 
                flow do
                  @addb1 = button "add", margin_left: 22
                  @addb1.click {newslate}
                  @removeb1 = button "remove"
                  @removeb1.click do
                    @numofslates = slatecount
                    if @numofslates <= 3
                      return nil
                    else
                      if viewing?("#{@slatelist_box.text}") == 1
                        previous = previousslate
                        set_viewing("#{previous}")
                      end
                      remove_slate("#{@slatelist_box.text}")
                      namesofslates = []
                      namesofslates = list_slates
                      @slatelist_box.items = nameofslates
                    end
                  end
                end
              #@cloneb1 = button "clone", width: 80, height: 35, margin_left: 22
                #@cloneb1.click {}
                #buttons section
            end
          end
=end
      end
      stack height: 250, width: 350 do #password and connectivity area
        stack height: 30 do
          #spacer
        end
=begin
        flow margin_left: 80 do
          stack width: 70 do 
            radio :networking
            para "Offline", stroke: "#BB27FF"
          end
          stack width: 80 do 
            radio :networking
            para "Online", stroke: "#BB27FF"
          end
        end
        @connectsettingsb1 = button "Connectivity Settings", margin_left: 100
        @connectsettingsb1.click{}
        stack height: 45 do
          #spacer
        end
=end
        para "Password Setup", margin_left: 113, stroke: "#BB27FF"
        stack height: 14 do
          #spacer
        end
        flow do 
          @check1 = check margin_left: 88; para "  Administrator", width: 160, stroke: "#BB27FF"

          if get_admin_status == 0
            @check1.checked = false
          elsif get_admin_status == 1
            @check1.checked = true
          end
          @editl1 = edit_line width: 6
          #@setb1.click {}
        end
      end
    end
    stack do #lock and seconds with ok and cancel buttons
      stack height: 80 do
        #spacer
      end
=begin
      flow do
        @lock_check = check margin_left: 160; para "Lock slate automatically after", width: 242, stroke: "#BB27FF"
        if get_lock_state == 0
          @lock_check.checked = false
        else
          @lock_check.checked = true
        end
        @lockmin = edit_line width: 27
        @lockmin.text = "#{get_lock_time}"
        para " minutes", width: 70, stroke: "#BB27FF"
      end
=end
      flow do
        flow width: 202 do end
=begin
        para "Update slate every", stroke: "#BB27FF", width: 170
        @pushmin = edit_line width: 27
        @pushmin.text = "#{get_update_time}"
        para "    seconds.", stroke: "#BB27FF", width: 280
=end
      end
        stack height: 58 do
          #spacer
        end
        flow do
          @okb1 = button "OK", height: 32, width: 60, margin_left: 308
          @okb1.click do
            passwd = @editl1.text
            passwdlength = passwd.length
            if @check1.checked? and passwdlength.between?(14,50)
               set_admin_status("1")
               set_admin_password("#{@editl1.text}")
               set_admin("0")
            else
               set_admin_status("0")
               @check1.checked = false
            end
            #set_update_time("#{@pushmin.text}")
          end
          #@cancelb1 = button "Cancel", height: 32, width: 70, margin_left: 250
          #@cancelb1.click{window.close}
        end
    end

  end
end
end












def acceptgifts
  window width: 600, height: 700, title: " " do
      background "#0E0E0E"
    stack do
      stack height: 25 do
        #spacer
      end
          
      stack do #send gift area
        para "Which gifts do you want to accept?", align: 'center', stroke: "#BB27FF"
        @giftsl1 = [ 'sam', 'wrat', 'sap', 'cor', 'singo', 'conna', 'bronno', 'slimp', 'sand', 'carpnn', 'wtor', 'stora', 'hnap', 'corap', 'slon' ]
        @giftsl1.map! do |gift|
          flow margin_left: 14 do 
            @c = check; para gift, width: 200, stroke: "#BB27FF"
          end
          [ @c, gift ]
        end
        stack height: 35 do
          #spacer
        end
        flow do
          @acceptb1 = button "Accept selected gifts", width: 158, height: 38, margin_left: 80
          @acceptb1.click{}
          @rejectb1 = button "Reject selected gifts", width: 150, height: 38, margin_left: 40
          @rejectb1.click{}
          #@cancelb1 = button "Cancel", width: 60, height: 32, margin_left: 70
          #@cancelb1.click{}
        end
      end
    end
  end
end

def accepthosts
  Shoes.app width: 600, height: 700, title: " " do
      background "#0E0E0E"
    stack do
      stack height: 25 do
        #spacer
      end
          
      stack do #send gift area
        para "Which hosts do you want to accept?", align: 'center', stroke: "#BB27FF"
        @hostsl1 = [ 'sam', 'wrat', 'sap', 'cor', 'singo', 'conna', 'bronno', 'slimp', 'sand', 'carpnn', 'wtor', 'stora', 'hnap', 'corap', 'slon' ]
        @hostsl1.map! do |host|
          flow margin_left: 14 do 
            @c = check; para host, width: 200, stroke: "#BB27FF"
          end
          [ @c, host ]
        end
        stack height: 35 do
          #spacer
        end
        flow do
          @acceptb1 = button "Accept selected hosts", width: 158, height: 38, margin_left: 80
          @acceptb1.click{}
          @rejectb1 = button "Reject selected hosts", width: 155, height: 38, margin_left: 40
          @rejectb1.click{}
          #@cancelb1 = button "Cancel", width: 60, height: 32, margin_left: 70
          #@cancelb1.click{}
        end
      end
    end
  end
end





def newslate
  Shoes.app title: "New Slate", width: 500, height: 260 do
      background "#0E0E0E"
    stack do
      stack height: 25 do
        #spacer
        end
      flow height: 150 do
        stack width: 350, height: 150 do #name and shortcut section
          flow do
            para "Name", width: 74, margin_left: 80, stroke: "#BB27FF"
            flow width: 60 do end
            @namee2 = edit_line width: 162
          end
          stack height: 17 do
            #spacer
          end
        end

=begin
        stack width: 250, height: 150 do
          #background lightgreen #password section
          border yellow, strokewidth: 2
          stack height: 30 do
            #spacer
          end
          flow width: 180 do
            radio :password
            para "No Password", width: 150, stroke: "#BB27FF"
          end
          flow height: 8 do
            #spacer
          end
          flow do
            radio :password
            para "Password", width: 84, stroke: "#BB27FF"
            @passworde1 = edit_line width: 8
          end
        end

      end
      stack do #send gift area
        para "Send gifts to the following hosts", margin_left: 32, stroke: "#BB27FF"
        @hostslist = [ 'sam', 'wrat', 'sap', 'cor', 'singo', 'conna', 'bronno', 'slimp', 'sand', 'carpnn', 'wtor', 'stora', 'hnap', 'corap', 'slon' ]
        @hostslist.map! do |name|
          flow margin_left: 14 do 
            @c = check; para name, width: 200, stroke: "#BB27FF"
          end
          [ @c, name ]
        end
=end
        flow do
          @okb1 = button "Ok", width: 40, height: 32, margin_left: 220
          @okb1.click do
            if is_arg_cmd?(@namee2.text) == true
              Billboard.alert_cmd_slatename_error
            else 
              new_slate(@namee2.text)
            end
          end
          #@cancelb1 = button "Cancel", width: 60, height: 32, margin_left: 80
          #@cancelb1.click{self.close}
        end
      end
    end
  end
end


def sendgifts
  Shoes.app title: 'Sending Gifts', height: 700, width: 800 do
  background "#0E0E0E"

    stack height: 100 do
      #spacer
    end
    flow width: 400 do
      para "Hosts", stroke: "#BB27FF", align: 'center'
      stack height: 40 do
        #spacer
      end
      stack do
        @hostsl1 = [ 'princess', 'demon', 'genie', 'angel', 'nymph' ]
        @hostsl1.map! do |host|
          flow { @c1 = check margin_left: 140; para host, margin_left: 7, width: 200, stroke: "#BB27FF" }
          [@c1, host]
        end
      end
    end
    flow width: 400 do
      para "Slates", stroke: "#BB27FF", align: 'center'
      stack height: 40 do
        #spacer
      end
      stack do
        @slatesl1 = [ 'Schedule', 'ideas', 'music', 'programming' ]
        @slatesl1.map! do |slate|
          flow { @c2 = check margin_left: 140; para slate, margin_left: 7, width: 200, stroke: "#BB27FF" }
          [@c2, slate]
        end
      end
    end
    stack height: 44 do
      #spacer
    end
    flow do
      @sendb1 = button "Send Gifts", height: 50, width: 88, margin_left: 300
      @sendb1.click{}
      #@backb1 = button "Back", height: 50, width: 45, margin_left: 30
      #@backb1.click{}
    end
  end
end


def setpassword
  window title: "password", width: 260, height: 200 do
    background bisque

  tagline "Set Password", align: "center", margin_top: 20
  flow height: 80 do
    edit_line width: 6, margin_top: 32, margin_left: 128
  end
    flow width: 200 do
      @b_ok = button "ok", width: 30, margin_left: 82
      @b_ok.click do
        #save password
      end
      #@b_cancel = button "cancel", width: 30, margin_left: 20
      #@b_cancel.click{}
    end
  end
end


def slatesettings(choosen)
  Shoes.app title: "Slate Settings", height: 700, width: 700 do
    background "#0E0E0E"
    @choosenslate = choosen
    @slateid = get_slateid("#{choosen}")
    stack do
      stack height: 57 do
        #spacer
      end
      flow do
        para "Name", width: 74, margin_left: 254, stroke: "#BB27FF"
        @namee1 = edit_line width: 162, margin_left: 246
        @namee1.text = @choosenslate
      end
      stack height: 17 do
        #spacer
      end
      stack height: 68 do
        #spacer
      end
=begin   #does not need password for each slate
      flow width: 550, margin_left: 110  do
        flow width: 230 do 
          radio :password
          para "No Password", width: 200, stroke: "#BB27FF"
        end
        flow width: 160 do
          radio :password
          para "Password", width: 110, stroke: "#BB27FF"
          @passworde1 = edit_line width: 11
        end
      end
=end
      stack height: 66
        #spacer
      end
      stack do #share with hosts area
        para "Select hosts to share with", align: 'center', stroke: "#BB27FF"
        @hostslist = [ 'sam', 'sap', 'cor', 'singo', 'slimp', 'sand', 'carpnn', 'wtor', 'stora', 'hnap', 'corap', 'slon' ]
        @hostslist.map! do |name|
          flow margin_left: 14 do
            @c = check; para name, width: 200, stroke: "#BB27FF"
          end
          [ @c, name ]
        end
        stack height: 20 do
          #spacer
        end
        flow do
          @okb1 = button "Ok", width: 40, height: 32, margin_left: 264
          @okb1.click{set_slatename(@slateid, @namee1.text)}
          #@cancelb1 = button "Cancel", width: 60, height: 32, margin_left: 80
          #@cancelb1.click{}
        end
    end
  end
end


begin

Shoes.app title: 'Slates', width: 450, height: 632 do
  background '#0E0E0E'
  
 @cmd_index = 0
 @seconds_after_keypress = 0
 @needs_updating = 0
 @focusoncommandline = 1
 set_cmd_view("0")
 
  every(1) do 
    if @seconds_after_keypress >= 3
      @needs_updating = 0
      @seconds_after_keypress = 0
      if using_cmd? == 0
        update_text("#{@slate_text_box.text}")
      end
    end
    if @needs_updating == 1
      @seconds_after_keypress += 1
    end
  end

  def tab_on_slatetext
    if using_cmd? == 0 && @needs_updating == 1
      update_text("#{@slate_text_box.text}")
      @needs_updating = 0
      @seconds_after_keypress = 0
    end
    @commandline.focus
    @focusoncommandline = 1
  end

  def tab_on_cmdline
    if using_cmd? == 0 && @needs_updating == 1
      update_text("#{@slate_text_box.text}")
      @needs_updating = 0
      @seconds_after_keypress = 0
    end
    @slate_text_box.focus
    @focusoncommandline = 0
  end

  def go_to_slate
    #find slate and switch to it
    set_cmd_view("0")
    if viewing?("#{@commandline.text}") == 1
      @slate_text_box.text = "#{set_maintext_viewing}"
    else
      set_viewing("#{@commandline.text}")
      @slate_text_box.text = "#{set_maintext_viewing}"
      @sname_flow.clear{@sname = para "#{get_slatename}", stroke: '#E055FF', size: 'small', margin_left: 4}
    end
  end

  def next_slate
    if using_cmd? == 0 && @needs_updating == 1
      update_text("#{@slate_text_box.text}")
      @needs_updating = 0
      @seconds_after_keypress = 0
    end
    @slate_text_box.text = "#{switch_maintext_R}"
    @sname_flow.clear{@sname = para "#{get_slatename}", stroke: '#E055FF', size: 'small', margin_left: 4}
    set_cmd_view("0") 
  end
 
  def previous_slate
    if using_cmd? == 0 && @needs_updating == 1
      update_text("#{@slate_text_box.text}")
      @needs_updating = 0
      @seconds_after_keypress = 0
    end
    @slate_text_box.text = "#{switch_maintext_L}"
    @sname_flow.clear{@sname = para "#{get_slatename}", stroke: '#E055FF', size: 'small', margin_left: 4}
    set_cmd_view("0") 
  end

  def view_slate
    set_cmd_view("0")
    @slate_text_box.text = "#{set_maintext_viewing}"
  end

  def process_command
#    if using_cmd? == 0 && @needs_updating == 1
#      update_text("#{@slate_text_box.text}")
#      @needs_updating = 0
#      @seconds_after_keypress = 0
#    end
    response = get_command("#{@commandline.text}")
    @slate_text_box.text = "#{response}"
    set_cmd_view("1")
    @sname_flow.clear{@sname = para "#{get_slatename}", stroke: '#E055FF', size: 'small', margin_left: 4}
  end

  def get_last_commands
    numolines = File.read(File.expand_path("../../log/history.log", __FILE__)).scan(/\n/).count
    @lines = []
    if numolines < 1
      return [ "" ]
    elsif numolines <= 22
      IO.readlines(File.expand_path("../../log/history.log", __FILE__)).each do |line|
        @lines << line
      end
      return @lines
    elsif numolines >= 23
      IO.readlines(File.expand_path("../../log/history.log", __FILE__))[-23..-1].each do |line|
        @lines << line
      end
      return @lines
    end
  end


  @commands = []
  #@commands = get_last_commands


  def reset_cmd_index
    @cmd_index = 0
  end

  def previous_cmd_index
    if @cmd_index == 0
      @cmd_index = 22
    else
      @cmd_index -= 1
    end
  end

  def next_cmd_index
    if @cmd_index == 22
      @cmd_index = 0
    else
      @cmd_index += 1
    end
  end

  def get_next_cmd
    next_cmd_index
    index = @cmd_index
    return @commands[index]
  end

  def get_previous_cmd
    previous_cmd_index
    index = @cmd_index
    return @commands[index]
  end

  def add_cmd(cmd)
    @commands.shift
    @commands.unshift(cmd)
    @commands.unshift("")
    if @commands.length > 23
      @commands.pop
    end
  end

  @keypresscommand = keypress do |k|
    @seconds_after_keypress = 0
    if k == "\t" and @focusoncommandline == 0
      tab_on_slatetext
    elsif k == "\t" and @focusoncommandline == 1
      tab_on_cmdline
    elsif k == "Up" and @focusoncommandline == 1
      @commandline.text = "#{get_next_cmd}"
    elsif k == "Down" and @focusoncommandline == 1
      @commandline.text = "#{get_previous_cmd}"
    elsif k == "\n" and @focusoncommandline.odd?
#      if @needs_updating == 1
#        if using_cmd? == 0
#          update_text("#{@slate_text_box.text}")
#          @needs_updating = 0
#        end
#      end 
      if is_slate?("#{@commandline.text}") == true
        go_to_slate
      elsif @commandline.text =~ /^next\s*$/ || @commandline.text =~ /^n\s*$/
        next_slate
      elsif @commandline.text =~ /^previous\s*$/ || @commandline.text =~ /^p\s*$/
        previous_slate
      elsif @commandline.text =~ /^v\s*$/ || @commandline.text =~ /^viewslate\s*$/
        view_slate 
      elsif @commandline.text =~ /^super\s*$/
        if get_admin_status == 1
          unlock_admin
        end
      else
        process_command
      end
      update_history("#{@commandline.text}")
      reset_cmd_index
      add_cmd("#{@commandline.text}")
      @commandline.text = ""
    elsif k == "Control_R"
      next_slate
    end
 
  end

    stack do

      @light_section = flow height: 40, width: 200 do
        background '#0E0E0E'
      end

      @command_section = flow height: 22 do
        #background '#0E0E0E'
      
        @sname_flow = flow width: 150 do
          @sname = para "#{get_slatename}", stroke: '#E055FF', size: 'small', margin_left: 4
        end
        @commandline_flow = flow width: 136, height: 22 do
          @commandline = edit_line width: 132, height: 22 do 
            @focusoncommandline = 1 
            if using_cmd? == 0 && @needs_updating == 1
              update_text("#{@slate_text_box.text}")
              @needs_updating = 0
              @seconds_after_keypress = 0
            end
          end
        end
        @buttons_flow = flow width: 162 do
          flow width: 72 do end
          @arrowb_flow = flow width: 90 do
            @bleft = image File.expand_path("../../images/arrowright_imagetwo.png", __FILE__)
            @bleft.click do
              previous_slate              
            end
            @bright = image File.expand_path("../../images/arrowright_image.png", __FILE__)
            @bright.click do
              next_slate
            end
          end
=begin
          @triplet_flow = flow width: 72 do
            flow height: 22, width: 24 do
              @glowgiftflow = flow hidden: true do
                @bglowgift = image "../images/gift_glow2.png"
                @bglowgift.click{admin_barrier('acceptgifts_win')}
              end
              @glowgiftflow.show  
            end
            @bkey = image "../images/key_glow.png"
            @bpencil = image "../images/pencil_glow_image_red.png"
          end
=end
        end
      end

      @slate = flow height: height, width: width do
        @slate_text_box = edit_box height: height, width: width  do
          @focusoncommandline = 0
          @needs_updating = 1
        end
        @slate_text_box.text = get_text
      end
 
      @new_set = flow height: 20 do
        #background '#0E0E0E'
        @bnewslate_flow = flow width: 450, height: 20 do
          @bnewslate = image File.expand_path("../../images/new_slate_icon.png", __FILE__)
          @bnewslate.click{admin_barrier('newslate_win')}
            flow width: 200 do end
         #@bsendgifts = image "../images/gift_nonglow2_bottom.png"
         #@bsendgifts.click{admin_barrier('sendgifts_win')}
            flow width: 210 do end
          @bsettings = image File.expand_path("../../images/settings_icon.png", __FILE__)
          @bsettings.click{admin_barrier('generalsettings_win')}
        end
      end
    end
  end

#rescue

#Shoes.app title: "Critical Error!" do
#  para "A critical error has occured which has caused this program to halt"
#end

#pop up error window in Shoes
ensure
#if there is no slate that is being viewed then make the first slate have a 1 for viewing
end




#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

