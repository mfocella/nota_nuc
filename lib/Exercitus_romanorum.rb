
path = File.expand_path("../../db", __FILE__)
$:.unshift path unless $:.include? path
require 'Quarrier.rb'

module Exercitus_romanorum
include Quarrier


####


GETTINGSTARTED = "\n  Getting started.  

	This program is meant provide you with a quick and easily accessable place to jot down all those thoughts that pop into your head.  The program allows you to create multiple sets of notes and move between them at any time using commands, the arrow buttons, or the Control Right key.  

	In this program each set of notes is called a slate.  A slate has a name and its associated text.  When you type something into a slate, it will automatically be saved 3 seconds after the last key was hit.  To switch between the command line at the top and the maintext area, you just have to hit the tab key.  The previous(or p) and next(or n) commands move you from slate to slate.  Typing in the name of a slate will bring you right to it.  The ls command lists all of your slates and the v command brings you back to the slates maintext.  To learn how to create, delete and rename slates using commands use the help command.  The program must have 3 slates at any given time.     

	To get the most out of this program, you should have it start every time your computer starts and leave it running against the right side of the screen.  You want the program to be available all the time so that when that thought, idea or note pops into your head you have a place to put it instantly.    
	        
	This is an ongoing project and I would love to hear any comments you may have.  You can e-mail any ideas, opinions or thoughts here: fakemail@google.com  This project is called nota_bene and can be found on bitbucket.org which is where you can can post any bug reports, contribute or fork the project.

https://bitbucket.org/mfocella/nota_nuc"

HELPTEXT = "\n  Help

The commands included with this program are:

next          #I bring you to the next slate
n                    #I am a shorter way to say next
previous    #I bring you to the previous slate
p                    #I am the short version of previous
viewslate  #I bring the a slates text back into view
v                   #I am an alias for viewslate
exit          #I shut the program down
e                    #I am an alias for exit
version      #I show you the version of this program you are running
lastlog		#I list the last part of the log file
history		#I show you what the last commands you used were
help(This is me)
about             #I give you a little more information about this program
demote		#I get rid of admin abilities
super		#I give admin abilities
rename		#I rename a slate
list             #I list your slates
ls                  #I am an alias for list
delete          #I delete stuff
d                    #I delete the command you are viewing
new                #I can be used to add new stuff

The above commands have a variety of different ways to be used.  To get more information on any of the commands, type in the command name and then help.

examples
list help
delete help
restore help




     To lock the program from others from removing and creating new slates, you can go to the general settings window which is accessed by clicking the button at the bottom right of the program. 
"



THANKYOUALL = "\n	Thank you matz for the lovely language that is ruby.  You had me at hello world.  This program also would not exist without those who have created such wonderful ruby libraries including ActiveRecord, Shoes, Logger, Sqlite3 and cucumber.  The pickax book, Eloquent Ruby, Practical Object Oriented Programming in Ruby, Build Awesome Command Line Applications in Ruby, and Designing with the Mind in Mind have been invaluable to my learning and have given me the knowledge to create this software you are using.  Thank You Bell Labs for the transistor!  \n\n	Anyone who is reading this and using my program, I hope this program makes your life easier.  I am working to make this the best it can be.  Thank you for your support.  \n\n	Mom and dad, you are always doing your best in guiding and helping me.  I love you.  I would like to thank my coworkers at AMEWAS, who inspired me to create such a program.  Steve Atkinson, you are amazing.  Your existance has brought me joy.  You are a great friend.  Andrew, I would not be here today if it were not for you.  I owe everything to you.  I don't think there exists enough lifetimes to show you what you mean to me.  For now, lets push forward.  


\n\n\n\nMark Focella"






  def is_arg_cmd?(arg)
    off_limit_args = [ "--slates", "--hosts", "new", "version", "delete", "rename", "lastlog", "history", "next", "previous", "exit", "demote", "super", "help", "about", "viewslate", "list", "ThankYou" ]
    off_limit_args.each do |cmd|
      if arg =~ /^#{cmd}\s*$/
        return true
      else
        #return nil
      end
    end
  end  

  def access?
        if get_admin_status == 1
      if is_admin? == 0
        return false
      end
    end
  end


  def is_slate?(command)
    slates = []
    slates = list_slates
    slates.each do |slate|
      if command =~ /^#{slate}\s*$/
        return true
      else
        #return nil
        next
      end
    end
  end

  def slatecount
    slates = []
    slates = list_slates
    number = slates.length
    return number
  end

  def cmd_no_ops?(recievecmd, matchcmd)
    if recievecmd =~ /^#{matchcmd}\s*$/
      return true
    else
      return false
    end
  end

  def cmd_option2?(recievecmd, matchcmd, flag1, flag2)
    if recievecmd =~ /^#{matchcmd}\s*(#{flag1}|#{flag2})\s*$/
      return true
    else
      return false
    end
  end

#below method isn't being used
  def cmd_flag1?(recievecmd, matchcmd, flag1)
    if recievecmd =~ /^#{matchcmd}\s*#{flag1}\s*/
      return true
    else
      nil
    end
  end
#########################


  def cmd_argument?(recievecmd, matchcmd, flag1, flag2)
    if recievecmd =~ /^#{matchcmd}\s*(#{flag1}|#{flag2})\s.*\s*$/
      return true 
    else
      nil
    end
  end

  

  def cmd_onlytwo_arguments?(recievecmd, matchcmd)
    if recievecmd =~ /^#{matchcmd}\s*\S+\s*\S+\s*$/
      return true
    else
      nil
    end
  end

  def cmd_get_arg1(recievecmd)
    arg1 = /^\S*\s*(\S*)\s*\S*\s*$/.match(recievecmd).captures
    return arg1
  end

  def cmd_get_arg2(recievecmd)
    arg2 = /^\S*\s*\S*\s*(\S*)\s*$/.match(recievecmd).captures
    #put in a check here if argument is equal to one of the commands or already existing slatenames
    return arg2
  end

  def cmd_get_argument(recievecmd, matchcmd)
    argument = /^#{matchcmd}\s.+\s(.+)\s*$/.match(recievecmd).captures
    #put in a check here if argument is equal to one of the commands or already existing slatenames and if so return error can't be cmd or slatename
    return argument
  end

  def arg_has_special?(arg)
    arg.match(/\W+/) { return true }
  end

  def is_arg_slate?(arg)
    slates = []
    slates = list_slates
    slates.each do |slate|
      if arg =~ /^#{slate}$/
        return true
      else
        #return nothing
        next
      end
    end
  end
    



  def use_cmd
    if using_cmd? == 0
      set_cmd_view("1")
    else
      nil
    end
  end
    
  def admin_check
    if is_admin == 0
      return "You must be an admin to use this command"
    else
    end
  end

  def get_command(command)
    #send the command to a file which will be used by the history command and up and down arrows scrolling through old commands that were run !!here
    use_cmd
    if command.length >= 400
      return "The length of the command must be less than 400 characters long"
    else
      if command =~ /^about/
        parse_about(command)
      elsif command =~ /^new/
        parse_new(command)
      elsif command =~ /^delete/
        parse_delete(command)
      elsif command =~ /^d\s*$/
        parse_delete(command)
      elsif command =~ /^ls/
        parse_ls(command)
      elsif command =~ /^list/
        parse_list(command)
      elsif command =~ /^ThankYou$/
        parse_thankyou(command)
      elsif command =~ /^rename/
        parse_rename(command)
      elsif command =~ /^exit/
        parse_exit(command)
      elsif command =~ /^e\s*$/
        parse_exit(command)
      elsif command =~ /^help/
        parse_help(command)
      elsif command =~ /^h\s*$/
        parse_help(command)
      elsif command =~ /^demote/
        parse_demote(command)
      elsif command =~ /^history/
        parse_history(command)
      elsif command =~ /^version/
        parse_version(command)
      elsif command =~ /^lastlog/
        parse_lastlog(command)
      elsif command =~ /^next\s*\S+\s*$/ || command =~ /^n\s*\S+\s*$/
        next_help
      elsif command =~ /^previous\s*\S+\s*$/ || command =~ /^p\s*\S+\s*$/
        previous_help
      elsif command =~ /^viewslate\s*\S+\s*$/ || command =~ /^v\s*\S+\s*$/
        viewslate_help
      elsif command =~ /^super\s*\S+\s*$/ 
        super_help
#     elsif command =~ /^print/
#        parse_print(command)
      else
        nocommand(command)
      #return "\n#{command}\n\n\n This is not one of the commands or slates.  Type help if you would like a list of commands.  To learn more about this program type about."
      end
    end
  end









private

  def nocommand(command)
    return "\n#{command}\n\n\n This is not a valid command or slate.  Type help if you would like a list of commands.  To learn more about this program type about."
  end

  def super_help
    return "\nsuper\n\n\nThis command asks for the administrator password so that you could become an admin.  If you are an admin, you will be able to access all the commands and sectons.  This command does not take any arguments.\n\nUsage: super"
  end

  def next_help
    return "\nnext\n\n\nThis command brings you to the next slate in your list of slates.  It does not take any arguments.  \n\nUsage: next\n\n\n\nAlias: n"
  end

  def previous_help
    return "\nprevious\n\n\nThis command brings you to the previous slate in your list of slates.  It does not take any arguments. \n\nUsage: previous \n\n\n\nAlias: p"
  end

  def viewslate_help
    return "\nviewslate\n\n\nThis command removes the command output and lets you view the slatetext.  It does not take any arguments.  \n\nUsage: viewslate\n\n\n\nAlias: v"
  end 

  def parse_version(command)
    match = "version"
    if cmd_no_ops?(command, match) == true
      ver = "\nYou are running Version 0.0.1 of the Slates program which is called gumshoe."
      return ver
    else
      usage = "\nversion\n\n\nThis command displays the version of this program that is running.  It does not take any arguments.  \n\nUsage: version"
      return usage
    end
  end

  def parse_about(command)
    match = "about"
    if cmd_no_ops?(command, match) == true
      return "\n#{GETTINGSTARTED}"
    else
      return "\nabout\n\n\nThis command gives you information about Slates.  It gives you an overview of the functions and features this program offers.  If you are new to Slates, I suggest you start here.  \n\nUsage: about"
    end
  end

  def parse_new(command)

    flag1 = "-s"
    flag2 = "--slate"
    match = "new"

  if access? == false
    return "\nYou must be an administrator to run this command."
  else
    if cmd_no_ops?(command, match) == true
      return "\nThe new command requires an option and an argument."
    elsif cmd_argument?(command, match, flag1, flag2) == true
      @argument = cmd_get_argument(command, match).join("")
      if arg_has_special?("#{@argument}") == true
        return "\nThe slatename can't contain special characters"
      elsif is_arg_slate?("#{@argument}") == true
        return "\nThe slate #{@argument} already exists."
      elsif @argument =~ /^(..|.)$/
        return "\nSlatename must be more than 2 characters long."
      elsif is_arg_cmd?(@argument) == true
        return "\nYour slate cannot be the name of a command."
      else
        new_slate("#{@argument}")
        return "\nThe slate #{@argument} has been created."
      end
    else
      return "\nnew\n\n\nThis command allows you to create a new slate.  \n\nUsage: new -s <slatename>  or new --slate <slatename> \n -s stands for slate.  You can only use one flag each time this command is run.\nThis command takes only one argument after the flag which is the name of the slate you want to create."
    end   
  end
  end

  def parse_delete(command)

    flag1 = "-s"
    flag2 = "--slate"
    match = "delete"  
    match2 = "d"

  if access? == false
    return "\nYou must be an administrator to run this command."
  else
    if cmd_no_ops?(command, match) == true  || cmd_no_ops?(command, match2) == true
      @numofslates = slatecount
      if @numofslates <= 3 
        return "\nYou cannot delete this slate, because there must be at least 3 slates at all times."
      else
        viewingslate = get_slatename
        previous = previousslate
        set_viewing("#{previous}")
        remove_slate("#{viewingslate}")
        return "\nThe slate #{viewingslate} was deleted."
      end
    elsif cmd_argument?(command, match, flag1, flag2) == true
      @argument = cmd_get_argument(command, match).join("")
      if arg_has_special?("#{@argument}") == true
        return "\nThere is no slate with the name #{@argument}"
      elsif is_arg_slate?("#{@argument}") == true
        @numofslates = slatecount
        if @numofslates <= 3
          return "\nYou cannot delete this slate, because there must be at least 3 slates at all times."
        end
        if viewing?("#{@argument}") == 1
          previous = previousslate
          set_viewing("#{previous}")
        end
        remove_slate("#{@argument}")
        return "\nThe slate #{@argument} was deleted."
      else
        return "\nThere is no slate with the name #{@argument}"
      end
    else
      return "\ndelete\n\n\nThis command allows you to delete a slate.  \n\nUsage: delete -s <slatename>  or  delete --slate <slatename>   \n -s stands for slate.  You can only use one flag each time this command is run.  Delete can only take one argument which is the name of the slate you would like to delete.  If you want to delete the slate you are viewing, just type delete or d without any arguments."
    end
  end
  end

  def parse_ls(command)
    match = "ls"
    if cmd_no_ops?(command, match) == true
      sap = list_slates.join("\n")
      @numofslates = slatecount
      far = "\n You have #{@numofslates} slates.\n\n\nYour slates include:\n\n #{sap}"
      return far
    else
      usage = "\nls\n\n\nThis command lists the slates.  It is a shortcut for list.  It doesn't not take any arguments or flags.\n\nUsage: ls"
      return usage
    end
  end

  def parse_list(command)
    
    match = "list"
    flag1 = "-s"
    flag2 = "--slates"
    @numofslates = slatecount

    if cmd_no_ops?(command, match) == true
      sap = list_slates.join("\n")
      hap = list_hosts.join("\n")
      slates = "\n You have #{@numofslates} slates.\n\n\nYour slates include: \n\n #{sap}"
      return slates
    elsif cmd_option2?(command, match, flag1, flag2) == true
      sap = list_slates.join("\n")
      slates = "\n You have #{@numofslates} slates.\n\n\nYour slates include: \n\n #{sap}"
      return slates
    else
      return "\nlist\n\n\nThis command lists the slates you have.  \n\nUsage: list [options]\n\t -s, --slates  list only slates\n\t \n\n\n The alias for list is: ls"
    end
  end

  def parse_thankyou(command)
    return "\n\n\n\n\n#{THANKYOUALL}"
  end

  def parse_rename(command)
    match = "rename"
  if access? == false
    return "\nYou must be an administrator to run this command."
  else
    if cmd_no_ops?(command, match) == true
    return "\nThe rename command requires two arguments.  The first argument must be an existing slatename and the other argument must be the name you want to change it to."
    elsif cmd_onlytwo_arguments?(command, match) == true
      oldname = cmd_get_arg1(command).join("")
      newname = cmd_get_arg2(command).join("")
      if newname =~ /^(..|.)$/
        return "\nThe slatename must be more than 2 characters long."
      elsif newname =~ /\W/
        return "\nYou can only use letters, numbers and underscores in the name of a slate."
      elsif oldname =~ /^help$/
        return "\nrename\n\n\nThis command renames an already existing slate to some other name. \n\nUsage:  rename <oldslatename> <newslatename>\n     This command must take in the name of an already existing slate and the name you want to change it to."
      elsif oldname =~ /\W/
        return "\nYou do not have a slate named: #{oldname}."
      elsif is_arg_cmd?(newname) == true
        return "\nYour slate cannot be the name of a command."
      elsif is_arg_slate?(newname) == true
        return "\nYou already have a slate called #{newname}."
      elsif is_arg_slate?(oldname) == true
        slateid = get_slateid("#{oldname}")
        set_slatename("#{slateid}", "#{newname}")
        return "\nThe slate #{oldname} has changed its name to #{newname}."
      else
        return "\nThe first argument #{oldname} is not the name of a slate."
      end
    else
      return "\nrename\n\n\nThis command renames an already existing slate to some other name. \n\nUsage:  rename <oldslatename> <newslatename>\n     This command must take in the name of an already existing slate and the name you want to change it to."
    end
  end
  end

  def parse_exit(command)
    match = "exit"
    if cmd_no_ops?(command, match) == true
      return exit
    elsif cmd_no_ops?(command, "e") == true
      return exit
    else
      usage = "\nexit\n\n\nThis command exits the program.  A shortcut for this is e.\n\nUsage:    exit"
      return usage
    end
  end

  def parse_help(command)
    match = "help"
    if cmd_no_ops?(command, match) == true
      return "\n#{HELPTEXT}"
    elsif cmd_no_ops?(command, "h") == true
      return "\n#{HELPTEXT}"
    else
      usage = "\nType in help if you need assistance."
      return usage
    end
  end

  def parse_print(command)
    match = "print"
    if cmd_no_ops?(command, match) == true
      #slatename = get_slatename
      #check that a file with the slatename does not exist already and if it does, change slatename to something else so that can print the slate
      #File.open("../../log/#{slatename}", "w+") { |f| f.write(set_maintext_viewing)}
      #use cups ruby gem to send a print job to the default printer using the file created above
      #file_to_print = Cups::PrintJob.new("file")
      #file_to_print.print
      #return "You have just sent a print job of the current slatetext to the default printer set up by cups"
      #send a message to logger and slate.log that print job send for current slate
      #until print == successful do nothing then send log message printed successfully and delete the file created for the print
      #use a loop to check the status of the print job with file_to_print.statusand if that value equals the completed status remove the print file and end the loop     
#use a ruby fork for above?
#make sure that the job printed and if it did, remove the file that was created with the main text
    else
      return "The print command does not take any arguments"
    end
  end

  def parse_demote(command)
    match = "demote"
    if cmd_no_ops?(command, match) == true
      set_admin("0")
      return "\nYou are no longer an administrator"
    else
      return "\ndemote\n\n\nThis command takes away administrator abilities.  \n\nUsage: demote"
    end
  end

  def parse_history(command)
    match = "history"
    if cmd_no_ops?(command, match) == true
      numolines = File.read(File.expand_path("../../log/history.log", __FILE__)).scan(/\n/).count

      #numolines = File.read("../log/history.log").scan(/\n/).count
      if numolines == 0
        return ""
      elsif numolines >= 50
        read = IO.readlines(File.expand_path("../../log/history.log", __FILE__))[-50..-1]
        return read.join("")
      else
        numolines *= -1 
        read = IO.readlines(File.expand_path("../../log/history.log", __FILE__))[numolines..-1]
        return read.join("")
      end
=begin
file.readlines.each do |line|
          count = 0
          if count <= 39
            lines << line
            #linetxt = "#{@period}" + "#{line}\n"
            #@period = "#{linetxt}"
            count += 1
          else
            linetxt = lines.join("\n\n\n\n")
            return "#{linetxt}"
          end
=end


    else
      return "\nhistory\n\n\nThis command gives you a listing of the last 50 commands that were run.  It does not take any arguments. \n\nUsage: history"
    end
  end

  def parse_lastlog(command)
    match = "lastlog"
    if cmd_no_ops?(command, match) == true

          numolines = File.read(File.expand_path("../../log/slate.log", __FILE__)).scan(/\n/).count
      if numolines == 0
        return ""
      elsif numolines >= 100
        read = IO.readlines(File.expand_path("../../log/slate.log", __FILE__))[-100..-1]
        return read.join("")
      else
        numolines *= -1
        read = IO.readlines(File.expand_path("../../log/slate.log", __FILE__))[numolines..-1]
        return read.join("")
      end
=begin
      lines = []
      File.open("../log/slate.log", "r") { |file|
        @period = ".START\n\n\n\n"
        file.readlines.each do |line| 
          count = 0
          if count <= 39
            lines << line 
            #linetxt = "#{@period}" + "#{line}\n"
            #@period = "#{linetxt}"
            count += 1
          else
            linetxt = lines.join("\n\n\n\n")
            return "#{linetxt}"
          end
        end
}
=end
    else
      return "\nlastlog\n\n\nThis command shows you the last few lines of this programs logfile.  It does not take any arguments.  \n\nUsage: lastlog"
    end
  end
  

end





#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

