class Error_Choose_Slate < Billboard
require 'green_shoes'


Shoes.app width: 180, height: 140, title: 'error' do
background tomato
  stack do
    para "ERROR", align: 'center'
    para "You must choose a slate"
    @b1 = button "back", margin_left: 68
    @b1.click{exit}
  end
end
end



#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

