=begin
green shoes
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
=end



#png_path = File.expand_path("../../png", __FILE__)
#puts "#{png_path}"
#$:.unshift png_path unless $:.include? png_path


module Billboard
require 'green_shoes'


  def self.alert_choose_slate
    Billboard.error_choose_slate
  end

  def alert_slatename_long
    Billboard.error_long_slatename
  end
  
  def self.alert_slatename_error                    #this is used
    Billboard.error_reguarding_slate
  end

  def alert_duplicate_slatename
    Billboard.error_duplicate_slatename
  end

  def alert_no_password
    Billboard.no_password
  end

  def alert_gifts_sent
    Billboard.gifts_sent
  end

  def alert_updatetime_length
    Billboard.error_updatetime_length
  end

  def alert_no_updatetime
    Billboard.no_updatetime
  end

  def self.alert_locktime_length
    Billboard.error_locktime_length
  end

  def alert_no_locktime
    Billboard.no_locktime
  end

  def alert_password_length
    Billboard.error_password_length
  end

  def alert_no_slatename
    Billboard.no_slatename
  end

  def self.alert_password_error               #this is used
    Billboard.error_password
  end
 
  def self.alert_updatetime_error             #this is used
    Billboard.error_updatetime
  end

  def self.alert_locktime_error               #this is used
    Billboard.error_locktime
  end
  
  def self.alert_hostname_error               #this is used
    Billboard.error_hostname
  end

  def self.alert_fatal_error
    Billboard.error_fatal
  end

  def self.alert_cmd_slatename_error
    Billboard.error_cmd
  end
 
  def self.alert_slate_created
    Billboard.slate_created
  end

private




  def Billboard.slate_created
    Shoes.app width: 220, height: 80 do
    background limegreen
      stack do
        stack height: 20 do end
        para "You have created a slate!", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end

  def Billboard.error_cmd
        Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        flow margin_left: 15 do
          @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
        end
        para "The slatename can't be the same as a command", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end


  def Billboard.error_fatal
    Shoes.app width: 380, height: 320, title: 'Fatal Error' do
      background tomato
        stack do
          para "Program Fatal Error", align: 'center'
          flow margin_left: 15 do
            @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
            @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
            @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)            
          end
          para "\nA deadly error has caused this program to crash.  \n\nTo report any bugs or issues you have with this application, you can contact !!!!!!!!!!! and report what happened"
        end
    end
  end

  def Billboard.error_choose_slate
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        flow margin_left: 15 do
          @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
        end
        para "You must choose a slate", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end

  def Billboard.error_hostname
    Shoes.app width: 280, height: 200, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        flow margin_left: 15 do
          @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
        end
        para "The hostname must be unique and must contain between 2 and 40 characters", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end


  def Billboard.error_updatetime
  ### ??? do I keep them as Shoes.app or change to window??? 
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        flow margin_left: 15 do
          @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
        end
        para "The updatetime must be between 1 and 99 seconds", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end

  def Billboard.error_locktime
  ### ??? do I keep them as Shoes.app or change to window??? 
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        flow margin_left: 15 do
          @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
        end
        para "The locktime must be between 1 and 99 minutes", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end

  def Billboard.error_password
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        flow margin_left: 15 do
          @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
        end
        para "The password must be between 14 and 50 characters long", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end


  def Billboard.error_reguarding_slate
  ### ??? do I keep them as Shoes.app or change to window??? 
    Shoes.app width: 220, height: 300, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        flow margin_left: 15 do
          @errorsymbol = image File.expand_path("../../images/error_symbol.png", __FILE__)
        end
        para "The slate name must be unique and its length must be between 2 and 18 characters.", align: 'center'
        #@b1 = button "back", margin_left: 68
        #@b1.click{exit}
      end
    end
  end

  
  def Billboard.error_long_slatename 
  
    Shoes.app width: 280, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        para "The slate name is too Long!\nThe name cannot be more than 18 characters", align: 'center'
        @b1 = button "back", margin_left: 153
        @b1.click{exit}
      end
    end
  end

  def Billboard.error_duplicate_slatename
  Shoes.app width: 220, height: 140, title: 'error' do
  background tomato
    stack do
      para "ERROR", align: 'center'
      para "The slatename must be unique"
      @b1 = button "back", margin_left: 68
      @b1.click{exit}
    end
  end
  end

  def Billboard.no_slatename
  Shoes.app width: 220, height: 140, title: 'error' do
  background tomato
    stack do
      para "ERROR", align: 'center'
      para "A name must be given for your slate"
      @b1 = button "back", margin_left: 68
      @b1.click{exit}
    end
  end
  end


  def Billboard.no_password
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        para "There is no password set to lock the slate"
        @b1 = button "back", margin_left: 68
        @b1.click{exit}
      end
    end
  end
  

  def Billboard.gifts_sent
    Shoes.app height: 150, width: 280, title: " " do
      background rosybrown
#    background "#BB27FF"
      stack height: 17 do
        #spacer
      end
      tagline "One or more gifts have been sent!", align: "center", stroke: "#0E0E0E"
      stack height: 11 do
        #spacer
      end
      @b1 = button "ok", margin_left: 126
      @b1.click{exit}
    end
  end


  def Billboard.error_updatetime_length
  Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        para "Slates must be updated between 1 and 99 seconds"
        @b1 = button "back", margin_left: 68
        @b1.click{exit}
      end
    end
  end

  def Billboard.no_updatetime
  Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        para "A time must be entered for slate updates"
        @b1 = button "back", margin_left: 68
        @b1.click{exit}
      end
    end
  end

    def Billboard.error_locktime_length
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        para "The lock slate time must be between 1 and 99 minutes."
        @b1 = button "back", margin_left: 68
        @b1.click{exit}
      end
    end
  end

  def Billboard.no_locktime
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        para "A time must be entered for when you want to lock the slates"
        @b1 = button "back", margin_left: 68
        @b1.click{exit}
      end
    end
  end

  def Billboard.error_password_length
    Shoes.app width: 220, height: 140, title: 'error' do
    background tomato
      stack do
        para "ERROR", align: 'center'
        para "The password must be between 14 and 50 characters"
        @b1 = button "back", margin_left: 68
        @b1.click{exit}
      end
    end
  end

#uncomment below for quick testing
#Billboard.error_password
#Billboard.error_fatal
end



#Developer : Mark Focella (m.focella@gmail.com)
#Date :  February 17, 2014

